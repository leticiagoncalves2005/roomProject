import OpenGL.GL as gl
import OpenGL.GLU as glu
import OpenGL.GLUT as glut

from math import sin, cos, pi
 
class Player(object):
    def __init__(self):
        #coordinates of the player
        self.x =  0.0
        self.y =  0.0
        self.z =  0.2
        self.polarAngle = 0
        self.azimuth = pi/2
        
        #coordinates of the torch
        self.zTorch = -0.3
        
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_AMBIENT,  [ 0.1,  0.1,  0.1,  1.0])
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_DIFFUSE,  [ 1.0,  1.0,  1.0,  1.0])
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_SPECULAR, [ 1.0,  1.0,  1.0,  1.0])
        gl.glLightf(gl.GL_LIGHT0, gl.GL_SPOT_EXPONENT, 8.0)
        gl.glLighti(gl.GL_LIGHT0, gl.GL_SPOT_CUTOFF, 25)
        gl.glLighti(gl.GL_LIGHT0, gl.GL_CONSTANT_ATTENUATION, 1)
        gl.glLighti(gl.GL_LIGHT0, gl.GL_LINEAR_ATTENUATION, 0)
        gl.glLightf(gl.GL_LIGHT0, gl.GL_QUADRATIC_ATTENUATION, 1)
      
        gl.glLightModelfv(gl.GL_LIGHT_MODEL_AMBIENT, [0.1, 0.1, 0.1, 1.0])
        
    def move(self, direction):
        angle = self.polarAngle
        if direction == 'front':
            self.polarAngle += 0
        elif direction == 'back':
            self.polarAngle += pi
        elif direction == 'right':
            self.polarAngle -= pi/2
        elif direction == 'left':
            self.polarAngle += pi/2
        self._moveFront()
        self.polarAngle = angle
        glut.glutPostRedisplay()
            
    def rotate(self, x, y):
            
        x = 2*x/glut.glutGet(glut.GLUT_WINDOW_WIDTH) - 1
        y = y/glut.glutGet(glut.GLUT_WINDOW_HEIGHT)
        if abs(x)>0.8:
            self.polarAngle -= x*pi/1000
        if y!=0 and y!=1:
            self.azimuth = y*pi
        glut.glutPostRedisplay()
            
    def _moveFront(self):
        tempX = self.x + cos(self.polarAngle)/20
        tempY = self.y + sin(self.polarAngle)/20
        if not (abs(tempX)>=1.4 or abs(tempY)>=1.4) :
            self.x = tempX
            self.y = tempY
    
    def draw(self):
        #position the torch
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_POSITION, [self.x,  self.y, self.zTorch,  1.0])
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_SPOT_DIRECTION, [sin(self.azimuth)*cos(self.polarAngle), sin(self.azimuth)*sin(self.polarAngle), cos(self.azimuth)])
        
    def setCamera(self, torch=False):
        gl.glLoadIdentity()
        glu.gluLookAt(self.x, self.y, (self.zTorch if torch else self.z),
                      sin(self.azimuth)*cos(self.polarAngle), sin(self.azimuth)*sin(self.polarAngle), cos(self.azimuth),
                      0.0, 0.0, 1.0)