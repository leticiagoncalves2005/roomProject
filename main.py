from __future__ import division
from __future__ import print_function

import sys

import OpenGL.GL as gl
import OpenGL.GLU as glu
import OpenGL.GLUT as glut

import objects
from shadowMapping import *

def init() :
    gl.glClearColor (0.0, 0.0, 0.0, 0.0)
    gl.glClearDepth (1.0)

    gl.glShadeModel (gl.GL_SMOOTH)

    gl.glEnable (gl.GL_LIGHTING)
    gl.glEnable (gl.GL_LIGHT0)
    gl.glEnable (gl.GL_DEPTH_TEST)
    gl.glLightModeli(gl.GL_LIGHT_MODEL_LOCAL_VIEWER, gl.GL_TRUE)
    #gl.glHint(gl.GL_PERSPECTIVE_CORRECTION_HINT, gl.GL_NICEST)
    glut.glutSetCursor(glut.GLUT_CURSOR_NONE)


def display() :
    global plr
    global shadow
    gl.glClear (gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

    plr.setCamera(torch = True)
    
    shadow.enableDepthCapture()
    objects.Room().draw()
    plr.draw()
    glut.glutSolidSphere(0.25, 16, 20)
    shadow.disableDepthCapture()
    
    plr.setCamera(torch = False)
    
    shadow.enableShadowTest()
    objects.Room().draw()
    plr.draw()
    glut.glutSolidSphere(0.25, 16, 20)
    shadow.disableShadowTest()

    glut.glutSwapBuffers();


def reshape(w, h) :
    gl.glViewport (0, 0, w, h)
    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity ()
    glu.gluPerspective(60.0, w/h, 0.05, 10.0)
    gl.glMatrixMode(gl.GL_MODELVIEW)

def keyboard(key, x, y) :
    global plr
    if key == glut.GLUT_KEY_UP :
        plr.move('front')
    elif key == glut.GLUT_KEY_DOWN :
        plr.move('back')
    elif key == glut.GLUT_KEY_LEFT :
        plr.move('left')
    elif key == glut.GLUT_KEY_RIGHT :
        plr.move('right')
    else :
        return
    glut.glutPostRedisplay()

def main() :
    _ = glut.glutInit(sys.argv)
    glut.glutInitDisplayMode(glut.GLUT_DOUBLE | glut.GLUT_RGB | glut.GLUT_DEPTH)

    glut.glutInitWindowSize(500, 500)
    glut.glutInitWindowPosition(100, 100)
    _ = glut.glutCreateWindow(b'Room')

    init()

    global plr
    plr = objects.Player()
    
    global shadow
    shadow = shadowMapping()
    
    _ = glut.glutDisplayFunc(display)
    _ = glut.glutReshapeFunc(reshape)
    _ = glut.glutSpecialFunc(keyboard)
    _ = glut.glutPassiveMotionFunc(plr.rotate)

    glut.glutMainLoop()


if __name__ == "__main__" :
    main()
